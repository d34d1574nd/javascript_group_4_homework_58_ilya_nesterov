import React from 'react';
import './Button.css';

const Button = props => {
    const btn = [
      {type: 'Button primary', label: 'Continue', clicked: props.continued},
      {type: 'Button danger', label: 'Close', clicked: props.closed},
      {type: 'Button dangerClose', label: 'x', clicked: props.closed},
    ];


 return (
   btn.map((item) => {
     return (
       <button
         key={item.type}
         onClick={item.clicked}
         className={item.type}
         type={item.type}
       >
         {item.label}
       </button>
     )
   })

 )
}



export default Button;