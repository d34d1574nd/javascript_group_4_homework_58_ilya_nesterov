import React, { Component } from 'react';
import './App.css';
import Button from "../components/UI/Button/Button";
import Modal from "../components/UI/Modal/Modal";
import Alert from "../components/UI/Alert/Alert";


class App extends Component {
  state = {
    purchasing: false,
    alertShow: false,
    alertShowFalse: false,
    button: false,
    alertType: [
      {type: 'Alert Primary', label: 'Все прошло успешно!', clicked: this.continued},
      {type: 'Alert Danger', label: 'Ошибка в выполнении данных', clicked: this.Cancel},
      {type: 'Alert Success', label: 'Все прошло успешно', clicked: this.continued},
      {type: 'Alert Warning', label: 'все прошло не удачно. Повторите попытку позже', clicked: this.Cancel},
    ],
  };

  purchase = () => {
    this.setState({purchasing: true})
  };

  purchaseCancel = () => {
    this.setState({purchasing: false});
    this.setState({alertShowFalse: true});
  };

  purchaseContinue = () => {
    this.setState({alertShow: true});
  };

  continued = () => {
    this.setState({alertShow: false});
  };

  Cancel = () => {
    this.setState({alertShowFalse: false});
  };



  render() {
    return (
      <div className="App">
        <button onClick={this.purchase}>Modal</button>
        <Modal
          show={this.state.purchasing}
          close={this.purchaseCancel}
          title='Title text'
        >
          <p className="text">Some text to modal window</p>
          <Button
            closed={this.purchaseCancel}
            continued={this.purchaseContinue}
          />
        </Modal>
        <Alert
          type={this.state.alertType[2].type}
          show={this.state.alertShow}
          class='Close'
          close={this.continued}
        >
          {this.state.alertType[2].label}
        </Alert>
        <Alert
          type={this.state.alertType[0].type}
          show={this.state.alertShow}
          class='Close'
          close={this.continued}
        >
          {this.state.alertType[0].label}
        </Alert>
        <Alert
          type={this.state.alertType[1].type}
          show={this.state.alertShowFalse}
          button={this.Cancel}
          class='closeAlert'
        >
          {this.state.alertType[1].label}
        </Alert>
        <Alert
          type={this.state.alertType[3].type}
          show={this.state.alertShowFalse}
          button={this.Cancel}
          class='closeAlert'
        >
          {this.state.alertType[3].label}
        </Alert>
      </div>
    );
  }
}

export default App;
